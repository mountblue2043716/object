function invert(obj) {
    invertedObj = {};
    for (let key in obj) {
        value = obj[key]
        invertedObj[value] = key;
    }
    return invertedObj;
}
module.exports = invert;
