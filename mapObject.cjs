function mapObject(obj, cb) {
    let transormedObject = {}
    for (key in obj) {
        transormedObject[key] = cb(obj[key]);
    }
    return transormedObject;
}
module.exports = mapObject;