function pairs(obj) {
    let pairList = []
    for (let key in obj) {
        let innerList = [];
        innerList.push(key);
        innerList.push(obj[key]);
        pairList.push(innerList);
    }
    return pairList;
}
module.exports = pairs;

